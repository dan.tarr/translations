{
	"httpMethodNotAllowed": "Il metodo HTTP \"{{method}}\" non è consentito in questo endpoint",
	"endpointNotFound": "Endpoint non trovato",
	"errorOccured": "Si è verificato un errore",
	"missingParameter": "{{parameter}} mancante",
	"invalidParameter": "{{parameter}} invalido",
	"invalidParameterType": "{{parameter}} deve essere un {{type}}",
	"parameterTooLong": "{{parameter}} non può essere più lungo di {{maxLen}} caratteri",
	"parameterTooShort": "{{parameter}} deve essere almeno di {{minLen}} caratteri",
	"parameterNotBetween": "{{parameter}} deve essere compreso tra {{min}} e {{max}}",
	"discordAccountNotLinked": "Un Account Discord deve essere collegato",
	"notLoggedIn": "Devi aver effetuato l'accesso per usare questo endpoint",
	"alreadyLoggedIn": "Hai già effettuato l'accesso",
	"invalidUsernameCharacters": "Il nome utente può contenere solo caratteri alfanumerici inglesi",
	"usernameTaken": "Nome utente già utilizzato",
	"passwordMismatch": "Le passwords non corrispondono",
	"changePasswordSuccess": "Password modificata correttamente",
	"resourceNotFound": "Un {{resource}} con quel {{property}} non è stato trovato",
	"resourceExists": "Un {{resource}} con questo {{property}} esiste già",
	"premiumFeature": "È necessario un abbonamento per l'utilizzo {{feature}}",
	"userNotFound": "Utente non trovato",
	"fileTooLarge": "File troppo grande. Il peso massimo di un file è {{size}}",
	"invalidFileFormat": "Formato del file non valido",
	"bodyTooLarge": "Il corpo del testo fornito è troppo grande",
	"bodyMalformed": "Il corpo del testo fornito possiede un formato errato",
	"authenticationApi": {
		"accountBanned": "Il tuo account è stato bannato. Motivazione: {{reason}}",
		"invalidForgotPasswordToken": "Il token del ripristino password non è valido oppure è scaduto",
		"passwordResetLinkSent": "Tentativo di invio di un link per la reimpostazione della password nei tuoi messaggi privati su Discord. Questo collegamento scadrà tra 24 ore.",
		"suspectedMultiAccounting": "Sembra che tu possieda molteplici account. Contatta l'assistenza di upload.systems se ritieni che si tratti di un errore.",
		"noReasonProvided": "Nessun motivo fornito",
		"invalidInviteCode": "Il codice di invito non esiste o è già stato utilizzato",
		"refreshTokenMismatch": "Il token aggiornato non corrisponde al token fornito",
		"passwordResetRequired": "Devi reimpostare la tua password",
		"notBetaUser": "Devi essere un utente iscritto alla beta per accedere a questo sito web",
		"mfaCodeRequired": "È richiesto il codice MFA",
		"failedCaptcha": "Captcha non riuscito, riprova"
	},
	"collectionsApi": {
		"invalidPublicityType": "La visibilità deve essere \"pubblica\" or \"privata\"",
		"collectionNotEmpty": "La raccolta deve essere vuota prima dell'eliminazione"
	},
	"discordApi": {
		"accountTooYoung": "Il tuo account Discord è nuovo per poter essere collegato",
		"accountNotFound": "Impossibile autenticarsi utilizzando Discord. Nessun account è associato a quell'account Discord"
	},
	"domainGroupsApi": {
		"usersMustBeArray": "Gli utenti devono essere un array del UUID",
		"noValidUsers": "Gli utenti devono contenere almeno 1 UUID valido",
		"invalidUserIds": "Sono stati forniti ID non validi",
		"groupInUse": "Questo gruppo di accesso è attualmente utilizzato da un dominio, impostalo per utilizzare un altro gruppo prima di eliminarlo"
	},
	"domainsApi": {
		"invalidTld": "Non accettiamo domini di primo livello simili, mi dispiace",
		"deletedDomain": "Dominio eliminato correttamente"
	},
	"imagesApi": {
		"futureTimestamp": "expiresAt deve essere un tempo del futuro",
		"unauthenticated": "Devi essere autenticato per usare questo endpoint",
		"invalidUploadKey": "Chiave non valida. Hai generato una chiave?",
		"subscriptionMaxFileSize": "Il file supera la dimensione massima consentita dall'abbonamento di {{size}}",
		"subscriptionMaxStorage": "Hai raggiunto il limite di spazio di archiviazione. Elimina alcuni file o valuta la possibilità di sottoscrivere un abbonamento.",
		"missingUploadPreferences": "Nessun dominio trovato, imposta le tue preferenze di caricamento nelle impostazioni",
		"userBanned": "Sei attualmente bannato, contatta il supporto di upload.systems se pensi che questo sia in errore",
		"unrecognisedIp": "Indirizzo IP non riconosciuto. Effettua il login sul sito e riprova.",
		"missingExtension": "Il file deve avere un'estensione",
		"premiumFileType": "È necessario un abbonamento superiore per caricare questo tipo di file",
		"spamUploadBan": "Sei stato bannato per aver caricato immagini troppo velocemente, contatta il supporto di upload.systems per presentare un appello",
		"noDeletePermissions": "Non hai l'autorizzazione per eliminare le immagini di un altro utente",
		"wipeInProgress": "Le tue immagini sono già in fase di eliminazione ({{percentage}}% complete)",
		"noImagesInCollection": "Nessuna immagine all'interno della raccolta fornita",
		"noValidImageIds": "Nessun ID valido fornito",
		"noImagesTo": "Non hai immagini da {{action}}",
		"archiveInProgress": "Hai già un archivio in fase di elaborazione",
		"archiveAlreadyRequested": "Hai già richiesto un archivio negli ultimi 7 giorni",
		"archiveTooLarge": "Non è possibile creare un archivio più grande di {{size}}",
		"reportOwnImage": "Non puoi segnalare le tue stesse immagini",
		"imageAlreadyReported": "Hai già segnalato questa immagine",
		"cannotSetFileAsShowcase": "Puoi impostare solo immagini o video come anteprima"
	},
	"imageWipesApi": {
		"wipeInProgress": "Hai già una eliminazione delle immagini in corso"
	},
	"mailAccountsApi": {
		"invalidDomain": "Dominio non valido",
		"maxAccounts": "Hai raggiunto il numero massimo di account di posta elettronica che puoi avere contemporaneamente",
		"maxAliases": "Hai raggiunto il numero massimo di alias di posta elettronica che puoi avere contemporaneamente",
		"emailInUse": "Indirizzo email già in uso",
		"aliasExists": "Esiste già un alias con quell'indirizzo email",
		"invalidCharacters": "Possiedi caratteri non validi nel tuo indirizzo email"
	},
	"notificationsApi": {
		"alreadyMarkedAsRead": "Tale notifica è già contrassegnata come letta"
	},
	"pastesApi": {
		"unsupportedLang": "Non è supportato il testo evidenziato della sintassi",
		"unsupportedType": "La tipologia di testo fornita non è supportata",
		"privateAndPasswordProtected": "Un testo non può essere sia privato sia protetto da password",
		"successfullyWiped": "Sono stati correttamente eliminati {{count}} testi",
		"passwordRequired": "Questo testo richiede una password per poter essere decriptato",
		"incorrectPassword": "Password errata",
		"notOwnPaste": "Puoi eliminare solo i tuoi testi"
	},
	"shortenUrlApi": {
		"invalidProtocol": "Protocollo URL non valido. Deve essere HTTP o HTTPS",
		"invalidIDCharacters": "L'ID può contenere solo caratteri inglesi e numerici",
		"selfRedirect": "L'URL non può reindirizzare a se stesso",
		"successfullyWiped": "Sono stati correttamente eliminati {{count}} URLs abbreviati",
		"notOwnShortenedUrl": "Puoi eliminare solo i tuoi URLs abbreviati",
		"blacklistedDomain": "Non puoi abbreviare questo URL perché il dominio è stato inserito nella blacklist"
	},
	"subscriptionsApi": {
		"notSelected": "Non hai selezionato un gruppo di abbonamento",
		"activeSubscription": "Hai già un abbonamento attivo",
		"groupMismatch": "L'utente fornito è attualmente iscritto al piano {{groupName}}, quindi non puoi regalargli questo piano",
		"selfGift": "Non puoi regalarti un abbonamento. Puoi invece aggiungere mesi nella pagina di gestione degli abbonamenti",
		"userNotGiftable": "Non puoi regalare un abbonamento a questo utente in questo momento, chiedigli di effettuare l'accesso prima",
		"noSubscriptionAddMonths": "Non hai un abbonamento attivo a cui aggiungere mesi",
		"noSubscriptionCancel": "Non hai un abbonamento attivo da annullare",
		"noSubscriptionResume": "Non hai un abbonamento attivo da riprendere",
		"pendingPlanChange": "Hai già una modifica dell'abbonamento in sospeso",
		"noDefaultPaymentMethod": "Devi avere almeno un metodo di pagamento allegato per continuare questo abbonamento",
		"cannotPreviewFree": "Non puoi visualizzare una modifica al piano gratuito, considera di annulla l'abbonamento",
		"cannotPreviewNoSubscription": "Non è possibile visualizzare una modifica senza un abbonamento attivo",
		"cannotPreviewSamePlan": "Non puoi visualizzare una modifica al tuo piano attuale",
		"cannotChangeAddedMonths": "Non puoi cambiare piano in questo momento perché hai aggiunto mesi al tuo abbonamento esistente. Puoi modificare il tuo piano dopo il {{date}}.",
		"cannotChangeGifted": "Non puoi cambiare piano in questo momento perché stai utilizzando un abbonamento in regalo. Puoi modificare il tuo piano dopo il {{date}}.",
		"discountCodeNotEligible": "Non sei idoneo per poter usare questo codice sconto"
	},
	"themesApi": {
		"noDeletePermissions": "Non hai l'autorizzazione per eliminare questo tema"
	},
	"userApi": {
		"privateProfile": "Il profilo di questo utente è privato",
		"noValidProperties": "Nessuna proprietà valida fornita",
		"multiLevelSubdomains": "I sottodomini multilivello non sono supportati. Usa - invece di .",
		"subdomainsNotAllowed": "I sottodomini non sono consentiti su questo dominio",
		"maxDomainCount": "Puoi avere fino a {{count}} domini selezionati contemporaneamente",
		"minDomainCount": "Devi avere almeno 2 domini selezionati contemporaneamente",
		"invalidDomain": "{{domain}} non è un dominio valido",
		"invalidSubdomainDomain": "I sottodomini non sono consentiti su {{domain}}",
		"autoChanceZero": "I valori forniti fanno sì che le probabilità Auto siano dello 0%",
		"randomChanceNot100": "Le possibilità dei domini casuali sommati devono arrivare a 100",
		"invalidUrl": "{{property}} deve essere un URL HTTP o HTTPS",
		"invalidEmbedFields": "Tutti i campi incorporati non possono essere vuoti. Si prega di compilare alcuni campi o di disabilitare gli Embeds di Discord",
		"noAuthorOrTitle": "Affinché Discord possa incorporare correttamente, è necessario un autore o un titolo",
		"invalidTimeformat": "il formato orario deve essere 12h o 24h",
		"cropAreaExceedsSize": "L'area di ritaglio non deve superare le dimensioni dell'immagine",
		"usernameChangeUnavailable": "Hai già cambiato il tuo nome utente negli ultimi 7 giorni",
		"unsupportedLanguage": "Lingua fornita non supportata",
		"invalidConfigType": "Tipologia di Configurazione non valida",
		"unprocessableGif": "Non siamo stati in grado di elaborare questa GIF",
		"fileAgeNegative": "L'età del file non può essere un valore negativo",
		"fileAgeTooBig": "L'età del file non può essere superiore a 12 mesi",
		"userCommentNotOwned": "Puoi eliminare solo i commenti che hai pubblicato",
		"cannotReportOwnComment": "Non puoi segnalare il tuo commento",
		"commentAlreadyReported": "Questo commento è già stato segnalato",
		"userCommentsDisabled": "Questo utente ha disabilitato i suoi commenti"
	},
	"userApis": {
		"MFAApi": {
			"mfaNotInitalised": "La MFA non è stata inizializzata",
			"mfaNotEnabled": "Non hai abilitato la MFA"
		},
		"paymentApi": {
			"atLeastOnePaymentMethodRequired": "Devi avere almeno un metodo di pagamento con un abbonamento attivo",
			"defaultPaymentMethod": "Non è possibile eliminare un metodo di pagamento contrassegnato come predefinito",
			"amountDecimalPlaces": "L'importo non può avere più di 2 cifre decimali",
			"amountTooSmall": "L'importo non può essere inferiore a {{amount}}",
			"amountTooLarge": "L'importo non può essere maggiore di {{amount}}"
		},
		"uploadPresetsApi": {
			"missingParameters": "Ci sono parametri mancanti nella tua richiesta",
			"maxPresets": "Puoi avere fino a 10 preset di preferenze alla volta"
		}
	}
}
